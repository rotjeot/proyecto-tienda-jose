from tkinter import *
import os
import sqlite3
import sys 

print (str(sys.argv))
username = str(sys.argv[1])
password = str(sys.argv[2])

lista_articulos=[]
compras = []
base = sqlite3.connect('usuarios.db')
puntero = base.cursor()
info = []
puntero.execute('SELECT * FROM users WHERE correo = ? AND pswrd = ?', (username, password))
for r in puntero:
    info.append(r)
    print(info)
id = info[0][0]
name = info[0][1]
correo = info[0][2]
contrasena = info[0][3]
master = info[0][4]
if info[0][2] != username or info[0][3] != password:
    exit()
base_tienda = sqlite3.connect('tienda.db')
puntero_tienda = base_tienda.cursor()

def main_store_screen():
    global main_screen
    global name
    main_screen = Tk()
    main_screen.title("Tienda")
    bv = "Bienvenido " + name
    Label(text=bv, bg="blue").grid(row=0, column=0)
    Button(text="carrito", command=carrito).grid(row=0, column=4)
    puntero_tienda.execute('SELECT * FROM articulos')
    for r in puntero_tienda:
        print(r)
        lista_articulos.append(r)

    height = len(lista_articulos)
    width = 4
    
    for i in range(1,height+1): 
        Label(text=lista_articulos[i-1][2]).grid(row=i, column=0)
        Label(text="Precio: $" + str(lista_articulos[i-1][4])).grid(row=i, column=1)
        Label(text="Disponibles: " + str(lista_articulos[i-1][5])).grid(row=i, column=2)
        im = str(lista_articulos[i-1][7])
        imagen = PhotoImage(file=im)
        Label(image=imagen, bd=0, height=50, width = 50).grid(row=i, column=3)
        Button( text="Añadir",command=lambda:add_carrito(i)).grid(row=i, column=4)
        print(i)
    main_screen.mainloop()

def add_carrito(i):
    print(i)
    compras.append(lista_articulos[i-1])
    print(compras)
    
def carrito():
    global carrito_screen
    carrito_screen = Toplevel(main_screen)
    carrito_screen.title("carrito")
    height = len(compras)
    width = 4
    precio = 0
    for i in range(1,height+1): 
        Label(carrito_screen,text=compras[i-1][2]).grid(row=i, column=0)
        Label(carrito_screen,text="Precio: $" + str(compras[i-1][4])).grid(row=i, column=1)
        im = str(compras[i-1][7])
        imagen = PhotoImage(file=im)
        Label(carrito_screen, image=imagen, bd=0, height=50, width = 50).grid(row=i, column=2)
        precio = precio + compras[i-1][4]
    Label(carrito_screen, text = "Total: $"+ str(precio)).grid(row =i+1 ,column=2)
    Button(carrito_screen, text = " Comprar", command = comprar).grid(row = i+2, column = 2)
    

def comprar():
    cont = 0
    for q in compras:
        cont = q[5] - 1
        idd = q[0]
        print(cont)
        puntero_tienda.execute('UPDATE articulos SET cantidad= ? WHERE i=?',(cont, idd))
        base_tienda.commit()

    global success_screen
    success_screen = Toplevel(carrito_screen)
    success_screen.title("Success")
    success_screen.geometry("150x100")
    Label(success_screen, text="Success").pack()
    Button(success_screen, text="OK", command=delete_success).pack()

def delete_success():
    success_screen.destroy()
    carrito_screen.destroy()


main_store_screen()