from tkinter import *
from tkinter.filedialog import askopenfilename
import os
import sqlite3
import sys 

print (str(sys.argv))
username = str(sys.argv[1])
password = str(sys.argv[2])

base = sqlite3.connect('usuarios.db')
puntero = base.cursor()
info = []
puntero.execute('SELECT * FROM users WHERE correo = ? AND pswrd = ?', (username, password))
for r in puntero:
    info.append(r)
    print(info)
id = info[0][0]
name = info[0][1]
correo = info[0][2]
contrasena = info[0][3]
master = info[0][4]
if info[0][2] != username or info[0][3] != password or master != 1:
    exit()
base_tienda = sqlite3.connect('tienda.db')
puntero_tienda = base_tienda.cursor()

def main_store_screen():
    global main_screen
    global name
    main_screen = Tk()
    #main_screen.geometry("300x250")
    main_screen.title("Tienda")
    bv = "Bienvenido " + name
    Label(text=bv, bg="blue").grid(row=0, column=0)
    Button(text="Agregar", command=agregar).grid(row=0, column=4)
    lista_articulos=[]
    puntero_tienda.execute('SELECT * FROM articulos')
    for r in puntero_tienda:
        print(r)
        lista_articulos.append(r)

    height = len(lista_articulos)
    width = 4
    for i in range(1,height+1): 
        Label(text=lista_articulos[i-1][2]).grid(row=i, column=0)
        Label(text="Precio: $" + str(lista_articulos[i-1][4])).grid(row=i, column=1)
        Label(text="Disponibles: " + str(lista_articulos[i-1][5])).grid(row=i, column=2)
        im =str(lista_articulos[i-1][7])
        imagen = PhotoImage(file=im)
        Label(image=imagen, bd=0, height=50, width = 50).grid(row=i, column=3)
        #Button( text="Modificar",command=lambda:modificar(i)).grid(row=i, column=4)
    main_screen.mainloop()

def modificar(i):
    print(i)

def agregar():
    global add_screen
    add_screen = Toplevel(main_screen)
    add_screen.title("Agregar")
    add_screen.geometry("300x500")
 
    global id_articulo
    global nombre_articulo
    global costo
    global compra
    global cantidad

    global id_articulo_entry
    global nombre_articulo_entry
    global costo_entry
    global compra_entry
    global cantidad_entry

    id_articulo = StringVar()
    nombre_articulo = StringVar()
    costo = StringVar()
    compra = StringVar()
    cantidad = StringVar()
 
    Label(add_screen, text="Ingresa la información", bg="blue").pack()
    Label(add_screen, text="").pack()

    ID_lable = Label(add_screen, text ="ID")
    ID_lable.pack()
    id_articulo_entry = Entry(add_screen, textvariable=id_articulo)
    id_articulo_entry.pack()

    nombre_lable = Label(add_screen, text="Nombre ")
    nombre_lable.pack()
    nombre_articulo_entry = Entry(add_screen, textvariable=nombre_articulo)
    nombre_articulo_entry.pack()

    costo_lable = Label(add_screen, text="Costo")
    costo_lable.pack()
    costo_entry = Entry(add_screen, textvariable=costo)
    costo_entry.pack()

    compra_lable= Label(add_screen, text="Compra")
    compra_lable.pack()
    compra_entry = Entry(add_screen, textvariable=compra)
    compra_entry.pack()

    cantidad_lable= Label(add_screen, text="Cantidad")
    cantidad_lable.pack()
    cantidad_entry = Entry(add_screen, textvariable=cantidad)
    cantidad_entry.pack()

    Button(add_screen,text="Agregar imagen", command=archivo).pack()

    Label(add_screen, text="").pack()
    Button(add_screen, text="Agregar", width=10, height=1, bg="blue", command = anadir_articulo).pack()


def anadir_articulo():
    
    id_info = id_articulo.get()
    nombre_info = nombre_articulo.get()
    consto_info = costo.get()
    compra_info = compra.get()
    contidad_info = cantidad.get()
    
    reg = []
    reg.append(id_info)
    reg.append(nombre_info)
    reg.append(consto_info)
    reg.append(compra_info)
    reg.append(contidad_info)
    reg.append("NA")
    reg.append(n)
    puntero_tienda.execute("INSERT INTO articulos(id, nombre, venta, compra, cantidad, departamento,image) VALUES (?, ?, ?, ?, ?, ?, ?)", reg)
    base_tienda.commit()
    add_screen.destroy()



def archivo():
    global n
    Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
    filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
    n = str(filename)
    c = "mv " + n +" /home/rotjeot/Escritorio/proyecto/imagenes"
    os.system(c)
    
main_store_screen()