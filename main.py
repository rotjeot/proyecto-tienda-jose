#import modules
 
from tkinter import *
import os
import sqlite3
 
base = sqlite3.connect('usuarios.db')
puntero = base.cursor()

# Designing window for registration
 
def register():
    global register_screen
    register_screen = Toplevel(main_screen)
    register_screen.title("Registro")
    register_screen.geometry("300x500")
 
    global username
    global password
    global nombre
    global nombre_entry
    global username_entry
    global password_entry
    nombre = StringVar()
    username = StringVar()
    password = StringVar()
 
    Label(register_screen, text="Ingresa la información", bg="blue").pack()
    Label(register_screen, text="").pack()
    nombre_lable = Label(register_screen, text ="Nombre *")
    nombre_lable.pack()
    nombre_entry = Entry(register_screen, textvariable=nombre)
    nombre_entry.pack()
    username_lable = Label(register_screen, text="Correo * ")
    username_lable.pack()
    username_entry = Entry(register_screen, textvariable=username)
    username_entry.pack()
    password_lable = Label(register_screen, text="Contraseña * ")
    password_lable.pack()
    password_entry = Entry(register_screen, textvariable=password, show='*')
    password_entry.pack()
    Label(register_screen, text="").pack()
    Button(register_screen, text="Registrar", width=10, height=1, bg="blue", command = register_user).pack()
 
 # Designing window for login 

def login():
    global login_screen
    login_screen = Toplevel(main_screen)
    login_screen.title("Login")
    login_screen.geometry("300x250")
    Label(login_screen, text="BIENVENIDO").pack()
    Label(login_screen, text="").pack()

    global username_verify
    global password_verify

    username_verify = StringVar()
    password_verify = StringVar()

    global username_login_entry
    global password_login_entry

    Label(login_screen, text="Correo * ").pack()
    username_login_entry = Entry(login_screen, textvariable=username_verify)
    username_login_entry.pack()
    Label(login_screen, text="").pack()
    Label(login_screen, text="Contraseña * ").pack()
    password_login_entry = Entry(login_screen, textvariable=password_verify, show= '*')
    password_login_entry.pack()
    Label(login_screen, text="").pack()
    Button(login_screen, text="Login", width=10, height=1, command = login_verify).pack()


def register_user():

    username_info = username.get()
    password_info = password.get()
    nombre_info = nombre.get()
    reg = []
    reg.append(nombre_info)
    reg.append(username_info)
    reg.append(password_info)
    reg.append(0)
    try:
        puntero.execute("INSERT INTO users(nombre, correo, pswrd,master) VALUES (?, ?, ?, ?)", reg)
        base.commit()
        username_entry.delete(0, END)
        password_entry.delete(0, END)
        nombre_entry.delete(0,END)
        Label(register_screen, text="Registro exitoso", fg="green", font=("calibri", 11)).pack()
    except sqlite3.IntegrityError:
        Label(register_screen, text="Registro fallido. Prueba otro correo", fg="green", font=("calibri", 11)).pack()


def login_verify():
    username1 = username_verify.get()
    password1 = password_verify.get()
    print(password1)
    print(type(password1))
    username_login_entry.delete(0, END)
    password_login_entry.delete(0, END)

    puntero.execute('SELECT * FROM users WHERE correo = ? AND pswrd = ?', (username1, password1))
    try:
        info =[]
        for r in puntero:
            info.append(r)
        print(info)
        id = info[0][0]
        nombre = info[0][1]
        correo = info[0][2]
        contrasena = info[0][3]
        master = info[0][4]
        if info[0][2] == username1 and info[0][3]==password1 and info[0][4] == 0:
            login_sucess()
            c = "python3 tienda.py "+ str(username1) +" "+ str(password1)
            login_screen.destroy()
            main_screen.destroy()
            os.system(c)
        elif info[0][2] == username1 and info[0][3]==password1 and info[0][4] == 1:
            login_sucess()
            c = "python3 master.py "+ str(username1) +" "+ str(password1)
            login_screen.destroy()
            main_screen.destroy()
            os.system(c)

    except:
        user_not_found()

# Designing popup for login success

def login_sucess():
    global login_success_screen
    login_success_screen = Toplevel(login_screen)
    login_success_screen.title("Success")
    login_success_screen.geometry("150x100")
    Label(login_success_screen, text="Login Success").pack()
    Button(login_success_screen, text="OK", command=delete_login_success).pack()

# Designing popup for user not found
def user_not_found():
    global user_not_found_screen
    user_not_found_screen = Toplevel(login_screen)
    user_not_found_screen.title("Error")
    user_not_found_screen.geometry("150x100")
    Label(user_not_found_screen, text="Usuario no encontrado\n o  \ncontraseña incorrecta").pack()
    Button(user_not_found_screen, text="OK", command=delete_user_not_found_screen).pack()

# Deleting popups

def delete_login_success():
    login_success_screen.destroy()


def delete_user_not_found_screen():
    user_not_found_screen.destroy()
# Designing Main(first) window

def main_account_screen():
    global main_screen
    main_screen = Tk()
    main_screen.geometry("300x250")
    main_screen.title("Login")
    Label(text="Selecciona una opción", bg="blue", width="300", height="2").pack()
    Label(text="").pack()
    Button(text="Login", height="2", width="30", command = login).pack()
    Label(text="").pack()
    Button(text="Registro", height="2", width="30", command=register).pack()

    main_screen.mainloop()


main_account_screen()